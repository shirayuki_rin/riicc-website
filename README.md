# RIICC Official Website

## Usage

### Windows

```
> set FLASK_APP="app.py"
> flask run
```

### Linux

```
$ export FLASK_APP="app.py"
$ flask run
```

In your favorite web browser, access the website at localhost:5000.

## Requirements

- Python 3
- The following Python packages
  - flask
  - flask_sqlalchemy
  - flask_login
  - flask_wtf
  - mysqlclient
