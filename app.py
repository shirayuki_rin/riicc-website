from flask import Flask, flash, jsonify, redirect, render_template, request, url_for
from flask_login import LoginManager, current_user, login_user, login_required, logout_user
from flask_sqlalchemy import SQLAlchemy
from forms import SignupForm, LoginForm

app = Flask(__name__)
app.secret_key = 'secretkeytbh'
app.config['SQLALCHEMY_DATABASE_URI'] = r'mysql://root:password@localhost/riicc'
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)


class User(db.Model):
    username = db.Column(db.String(80), primary_key=True, unique=True)
    email = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))
    bio = db.Column(db.String(1000))
    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password
        self.bio = "test"
    def __repr__(self):
        return '<User {}>'.format(self.username)
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return str(self.username)

class Event(db.Model):
    title = db.Column(db.String(80), primary_key=True)
    start = db.Column(db.String(80), nullable=False)
    end = db.Column(db.String(80))
    def __init__(self, title, start, end = None):
        self.title = title
        self.start = start
        self.end = end
    def __repr__(self):
        return '<Event {}>'.format(self.title)
    @property
    def serialize(self):
        return {'title': self.title, 'start': self.start, 'end': self.end} if self.end else {'title': self.title, 'start': self.start}

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/calendar/')
@login_required
def calendar():
    return render_template('calendar.html')

@app.route('/calendar_data/')
@login_required
def calendar_data():
    return jsonify([i.serialize for i in Event.query.all()])

@app.route('/profile/')
@login_required
def profile():
    username = request.args.get('username', default = current_user.username)
    return render_template('profile.html', user = User.query.filter_by(username = username).first())

@app.route('/edit_profile/', methods=['GET', 'POST'])
@login_required
def edit_profile():
    if request.method == 'GET':
        return render_template('edit_profile.html')
    elif request.method == 'POST':
        user = current_user
        user.bio = request.form['bio']
        db.session.commit()
        flash('Profile updated')
        return redirect(url_for('profile'))

@login_manager.user_loader
def load_user(username):
    return User.query.filter_by(username = username).first()

@app.route('/signup/', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if request.method == 'GET':
        return render_template('signup.html', form = form)
    elif request.method == 'POST':
        if form.validate_on_submit():
            if User.query.filter_by(username=form.username.data).first():
                flash('Username already exists')
                return redirect(url_for('signup'))
            elif User.query.filter_by(email=form.email.data).first():
                flash('Email address already exists')
                return redirect(url_for('signup'))
            elif form.password.data != form.confirm_password.data:
                flash('Passwords are not the same')
                return redirect(url_for('signup'))
            else:
                newuser = User(form.username.data, form.email.data, form.password.data)
                db.session.add(newuser)
                db.session.commit()
                flash('User created!')
                return redirect(url_for('index'))
        else:
            flash('Form didn\'t validate')
            return redirect(url_for('signup'))

@app.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'GET':
        return render_template('login.html', form = form)
    elif request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter_by(username = form.username.data).first()
            if user:
                if user.password == form.password.data:
                    login_user(user)
                    flash('You\'re logged in')
                    return redirect(url_for('index'))
            else:
                flash('User doesn\'t exist')
                return redirect(url_for('login'))
        else:
            flash('Form didn\'t validate')
            return redirect(url_for('login'))

@app.route("/logout/")
@login_required
def logout():
    logout_user()
    flash('You\'re logged out')
    return redirect(url_for('index'))

db.init_app(app)
db.app = app
db.create_all()
app.run(debug = True)
